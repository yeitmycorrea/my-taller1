README Taller 1 DOCKER

Ejercicio AES- Modelado y validación de Arquitectura

Desarrollo Realizado:

Se creó un programa PHP que crea un servicio REST que retorna el valor de la suma de 2 números. 
El servicio es desplegado en una imagen Docker para su consumo.

Programa:  suma.php

Entradas: num1 y num2
    
Salida: "la suma de $num1 + $num2 es: $res"


Instrucciones de Ejecución

Para realizar su ejecución realice los siguientes pasos:

1.Ingrese a la consola y descargue el repositorio de gitlab al ambiente local con el siguiente comando:

git clone https://gitlab.com/yeitmycorrea/my-taller1.git

2.Ejecute el archivo Start.sh para iniciar la imagen

sh start.sh

3.Ingrese al navegador y colocar la siguiente ruta 

http://localhost/my-taller1/suma.php

4.En la ruta URL agregar los parametros num1 y num2 de la siguiente forma, el valor de las entradas num1 y num2 es dinámico:

http://localhost/my-taller1/suma.php?num1=3&num2=7

5.Ejecute el archivo stop.sh para detener la imagen

sh stop.sh